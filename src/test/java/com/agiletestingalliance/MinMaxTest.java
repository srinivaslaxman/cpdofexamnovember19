package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
	@Test
	public void testGt1() throws Exception {
	int result = new MinMax().findGreater(20,10);	
	assertEquals ("Testing for greater", 20, result);
	}
	@Test	
	public void testGt2() throws Exception {
        int result = new MinMax().findGreater(10,20);
        assertEquals ("Testing for greater", 20, result);
        }
}

